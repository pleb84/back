require('dotenv').config();
const express = require('express'); //referencia al paquete express
const userfile = require('./user.json');
const userfile2 = require('./Data/account.json');//para login y logout
const bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
var port = process.env.PORT || 3000;
const URL_BASE = '/api-peru/v1/';
const cors = require('cors');
//----------------------------------------------------
const requestJSON = require('request-json');
const baseMLabURL = 'https://api.mlab.com/api/1/databases/techu16db/collections/';
//const apikeyMLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
const apikeyMLab = 'apiKey=' + process.env.MLAB_APIKEY;
app.use(cors());
app.options('*',cors());
/*
https://api.mlab.com/api/1/databases/techu16db/collections/user?
f={"last_name": 1,"email":1,"_id":0}&s={"last_name":-1}&
apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF
*/
//GET PARA MONGODB
// GET users a través de mLab
app.get(URL_BASE + 'users',
  function(req, res) {
    const httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
      const fieldParam = 'f={"_id":0}&';//plantillas/interpolacion string
  //  const fieldParam = 'f={"last_name": 1,"email":1,"_id":0}&s={"last_name":-1}&';
   httpClient.get('user?' + fieldParam + apikeyMLab,
  //   client.get('user?' + fieldParam + apikeyMLab,
      function(err, respuestaMLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ' + respuestaMLab);
        console.log('Body: ' + JSON.stringify(body[0]));
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontradoX."};
            res.status(404);
          }
        }
        res.send(response);
      }); //httpClient.get(
}); //app.get(


//-----------------------------------------------------------------------
  app.get(URL_BASE + 'users/:id',
    function(req, res) {
      const httpClient = requestJSON.createClient(baseMLabURL);
      console.log("Cliente HTTP mLab creado.");
        let ind = req.params.id;
        const fieldParam = 'f={"_id":0}&';//plantillas/interpolacion string
        const fieldString = 'q={"userID":' + ind + '}&';
     httpClient.get('user?' + fieldString + fieldParam + apikeyMLab,
    //   client.get('user?' + fieldParam + apikeyMLab,
        function(err, respuestaMLab, body) {
          console.log('Error: ' + err);
          console.log('Respuesta MLab: ' + respuestaMLab);
          console.log('Body: ' + JSON.stringify(body[0]));
          var response = {};
          if(err) {
              response = {"msg" : "Error al recuperar users de mLab."}
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {"msg" : "Usuario no encontradoXX."};
              res.status(404);
            }
          }
          res.send(response);
        }); //httpClient.get(
  }); //app.get(

//-----------------------------------------------------------------------
//Operacion POST para mLab
app.post(URL_BASE + "users",
  function(req,res){
    const httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado - POST.");
    httpClient.get('user?' + apikeyMLab,
    function(err, respuestaMLab, body){
      newID=body.length+1;
      console.log("newID:" + newID);
      var newUser = {
        "userID" : newID+1,
        "first_name" : req.body.first_name,
        "last_name"  : req.body.last_name,
        "email"      : req.body.email,
        "password"   : req.body.password
       };
      httpClient.post(baseMLabURL + "user?" + apikeyMLab, newUser,
          function(error, respuestaMLab, body) {
            res.send(body);
          });
      });
  });
//-----------------------------------------------------------------------
//operacion delete para mLab
app.delete(URL_BASE + 'users/:id',
   function(req,res){
      var response = {};
      var id=req.params.id;
      var queryStringID='q={"userID":' + id + '}&';
      var httpClient = requestJSON.createClient(baseMLabURL);
      httpClient.get('user?'+queryStringID+apikeyMLab,
         function(error,respuestaMLab,body){
           if(error) {
               response = {"msg" : "Error al recuperar users de mLab."}
               res.status(500);
               res.send(response);
           } else {
             if(body.length > 0) {
                var respuesta = body[0];
                httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid + '?' + apikeyMLab,
                function(error,respuestaMLab,body){
                    response = {"Usuario eliminado" : body};
                    res.send(response);
                      });
             } else {
               response = {"msg" : "Usuario no encontradoX - Ope. Delete"};
               res.status(404);
               res.send(response);
             }
           }
        });
      });
//-----------------------------------------------------------------------
//operacion put para mLab
app.put(URL_BASE + 'users/:id',
 function(req,res){
    var id=req.params.id;
    var queryStringID='q={"userID":' + id + '}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryStringID + apikeyMLab,
       function(error,respuestaMLab,body){
         if(error) {
             response = {"msg" : "Error al recuperar users de mLab."}
             res.status(500);
             res.send(response);
         }else {
           if(body.length > 0) {
              var atrib = {"first_name": req.body.first_name,
                           "password": req.body.password };
//              var atrib = {"pais": "peru"};
//              var cambio = '{"$unset":' + JSON.stringify(atrib) +  '}';
              var cambio = '{"$set":' + JSON.stringify(atrib) +  '}';
              httpClient.put(baseMLabURL + 'user?' + queryStringID + apikeyMLab,JSON.parse(cambio),
                  function(error,respuestaMLab,body){
                      res.send(body);
                  });
           } else {
             response = {"msg" : "Usuario no encontradoX - Ope. Put"};
             res.status(404);
             res.send(response);
           }
         }
     });
 });
//**FIN OPERACIONES CON MONGODB

//Operacion GET de todos los usuarios (users.json)
//forma de llamar en postman http://localhost:3000/api-peru/v1/users
app.get(URL_BASE + 'users/json/doc',
    function(request,response) {
      response.status(200);
      console.log('get de user.json con ruta users/json/doc')
      response.send(userfile);
});

//Operacion GET a un usuario con ID (INSTANCE)
//forma de llamar en postman http://localhost:3000/api-peru/v1/users/8
//el 8 al final porque llamamos al id=8...puede ser otro
/*app.get(URL_BASE + 'users/:id',
    function(request,response) {
      let indice = request.params.id;
      console.log(indice);
      response.status(200);
      response.send(userfile[indice-1]);
});*/

//la diferencia con el anterior es que valida si el iduser existe
//corre ok...en postman http://localhost:3000/api-peru/v1/users/20
//http://localhost:3000/api-peru/v1/users/2
app.get(URL_BASE + 'users/json/doc/:id',
    function(request,response) {
      console.log(request.query);
      let indice = request.params.id;
      let respuesta =
        (userfile[indice-1] == undefined) ? {"msg":"No existe"} : userfile[indice-1]
        response.status(200).send(respuesta);
});

//muestra la cantidad de registros
//en postman http://localhost:3000/api-peru/v1/userstotal
app.get(URL_BASE + 'userstotal',
    function(request, response) {
      console.log(request.query);
      let total = userfile.length;
  //    let totalJson = JSON.stringify({num_elem : total});
  //    response.status(200).send(totalJson); tbm es valido
        response.status(200).send({"total": total});
 });

app.post(URL_BASE + 'users',
  function (req,res){
//    console.log(req.body);
//    console.log(req.body.id);
    let pos = userfile.length;
    let newUser = {
  //  id: req.body.id,
      id: ++pos,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      password: req.body.password
    }
    userfile.push(newUser);
    res.status(201);
    res.send({"mensaje":"Usuario creado correctamente",
              "Usuario":newUser,
              "userfile actualizado":userfile});
  });

//tarea put (1ra forma)
app.put(URL_BASE + 'users/:id',
    function(req,res) {
      let indice = req.params.id;
      var a;
      if (userfile[indice-1] == undefined){
          a = {"msg":"No existe"} ;
        } else {
         let modUser = {
      //id:  + indice, el operador + convierte una cadena a numero
         id: Number(indice),
         first_name: req.body.first_name,
         last_name: req.body.last_name,
         email: req.body.email,
         password: req.body.password
       }
      userfile.splice(indice-1,1,modUser);
      a = {"usuario actualizado":modUser,
           "userfile es":userfile} ;
    }
      res.status(201).send(a);
});

//tarea put - (2da forma)
app.put(URL_BASE + 'users/v2/:id',
    function(req,res) {
      let id = req.params.id;
      var a,indice;
      var encontro = false;
      for (i=0;i<userfile.length && encontro == false;i++){
          if(userfile[i].id == Number(id)){
             encontro = true;
             indice = i;
          }
      }

      if (encontro){
         userfile[indice].first_name = req.body.first_name ;
         userfile[indice].last_name = req.body.last_name ;
         userfile[indice].email = req.body.email ;
         userfile[indice].password = req.body.password ;
         a = {"usuario actualizado":userfile[indice],
              "userfile es":userfile} ;
      } else{
         a = {"msg":"No existe"} ;
      }
      res.status(201).send(a);
});

//delete: http://localhost:3000/api-peru/v1/users/3
app.delete(URL_BASE + 'users/:id',
    function(request,response) {
  //    onsole.log(request.query);
      let indice = request.params.id;
      let respuesta =
        (userfile[indice-1] == undefined) ?
        {"msg":"No existe"} :
        {"Usuario eliminado" : (userfile.splice(indice-1,1))}
        response.status(200).send(respuesta);
});

//---------------------------------------------------------------------------
// LOGIN - /Data/user.json
 app.post(URL_BASE + 'login',
  function(request, response) {
  // console.log("POST /apitechu/v1/login");
   console.log(request.body.email);
   console.log(request.body.password);
   var a = 0;
   var user = request.body.email;
   var pass = request.body.password;
   for(us of userfile2) {
    if(us.email == user) {
       a = 1;
     if(us.password == pass) {
      us.logged = true;
      writeUserDataToFile(userfile2);
      console.log("Login correcto!");
      response.send({"mensaje" : "Login correcto.",
              "idUsuario" : us.userID,
               "logged" : "true"});
     } else {
      console.log("Login incorrecto.");
      response.send({"mensaje" : "Login incorrecto."});
     }
    }
   }
   if (a == 0){
     console.log("Login Incorrecto.");
     response.send({"mensaje" : "Login Incorrecto."});
   }
 });
 // LOGOUT - /Data/user.json
 app.post(URL_BASE + 'logout',
  function(request, response) {
   //console.log("POST /apitechu/v1/logout");
   var a=0;
   var userId = request.body.id;
   console.log(userId);
   for(us of userfile2) {
    if(us.userID == userId) {
       a = 1;
     if(us.logged) {
      delete us.logged; // borramos propiedad 'logged'
      writeUserDataToFile(userfile2);
      console.log("Logout correcto!");
      response.send({"mensaje" : "Logout correcto.", "idUsuario" : us.userID});
     } else {
      console.log("Logout incorrecto.");
      response.send({"mensaje" : "Logout incorrecto."});
     }
    }
   }
   if (a == 0){
     console.log("Logout Incorrecto.");
     response.send({"mensaje" : "Logout Incorrecto."});
   }
 });

 function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./Data/user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
    if(err) {
     console.log(err);
    } else {
     console.log("Datos escritos en './Data/user.json'.");
    }
   })
 }

//Servidor escuchara en la URL (Servidor Local)
app.listen(port,function(){
    console.log('Node JS escuchando desde puerto 3000...');
    console.log('Node JS escuchando desde puerto 2000...');
    console.log('Node JS escuchando desde puerto 1000...');
});
