# Imagen inicial base/inicial
FROM node:latest

# Crear directorio de trabajo del contenedor Docker
WORKDIR /docker-api-peru

# Copiar archivos del proyecto en directorio del contenedor
ADD . /docker-api-peru

# Instalar dependencias de producciòn
#RUN npm install --only=production

# Exponer puerto escucha del contenedor (mismo definido en API REST)
EXPOSE 3000

# Lanzar comandos necesarios para ejecutar nuestra API
# CMD ["node", "server.js"]
CMD ["npm", "run", "pro"]
